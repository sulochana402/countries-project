# Countries Information App

This is a React application that displays information about countries using the REST Countries API. The application supports dark mode and includes functionality to filter and sort countries by name, population, and area.

## Features

- Fetches and displays data from the REST Countries API.
- Search functionality to filter countries by name.
- Filter by region and subregion.
- Sort countries by population and area.
- Dark mode toggle.

## Components

- App
- Homepage
- Countries
- FilterBySubregion
- CountryDetails
