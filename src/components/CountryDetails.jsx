import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { DarkModeContext, useContext } from "./DarkModeContext";
import "./countryDetails.css";

function CountryDetails() {
  const { id } = useParams();

  const [country, setCountry] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [borderCountries, setBorderCountries] = useState([]);
  const { darkMode, toggleDarkMode } = useContext(DarkModeContext);

  useEffect(() => {
    if (!id) {
      return;
    }

    setLoading(true);
    fetch(`https://restcountries.com/v3.1/alpha/${id}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Failed to fetch country data");
        }
        return response.json();
      })
      .then((data) => {
        setCountry(data[0]);
        if (data[0].borders && data[0].borders.length > 0) {
          fetchBorders(data[0].borders);
        }
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }, [id]);

  const fetchBorders = (borders) => {
    Promise.all(
      borders.map((code) =>
        fetch(`https://restcountries.com/v3.1/alpha/${code}`).then((response) => response.json())
      )
    )
      .then((data) => {
        const borderNames = data.map((country) => country[0].name.common);
        setBorderCountries(borderNames);
      })
      .catch((error) => {
        console.error("Error fetching border countries:", error);
      });
  };
  

  if (loading) {
    return <div className="loadingState">Loading...</div>;
  }

  if (error) {
    return <div className="errorState">Error: {error.message}</div>;
  }

  if (!country) {
    return <div className="emptyState">No country found.</div>;
  }

  const renderNativeNames = () => {
    if (!country || !country.name || !country.name.nativeName) {
      return null;
    }
    const nativeNames = country.name.nativeName;
    return (
      <span className={`native-names ${darkMode ? "dark-list" : ""}`}>
        {Object.keys(nativeNames).map((key, index) => (
          <span key={key} className="native-name-item">
            {nativeNames[key].official}
            {index < Object.keys(nativeNames).length - 1 && ", "}
          </span>
        ))}
      </span>
    );
  };

  const renderTopLevelDomains = () => {
    if (!country || !country.tld) {
      return null;
    }
    return (
      <span className="tld-list">
        {country.tld.map((domain, index) => (
          <span key={index} className={`tld-item ${darkMode ? "dark-list" : ""}`}>
            {domain}
            {index < country.tld.length - 1 && ", "}
          </span>
        ))}
      </span>
    );
  };

  const renderCurrencies = () => {
    if (!country || !country.currencies) {
      return null;
    }
    const currencies = country.currencies;
    return (
      <span className="currency-list">
        {Object.keys(currencies).map((key, index) => (
          <span key={key} className={`currency-item ${darkMode ? "dark-list" : ""}`}>
            {currencies[key].name} ({currencies[key].symbol})
            {index < Object.keys(currencies).length - 1 && ", "}
          </span>
        ))}
      </span>
    );
  };

  const renderLanguages = () => {
    if (!country || !country.languages) {
      return null;
    }
    const languages = country.languages;
    return (
      <span className="language-list">
        {Object.values(languages).map((language, index) => (
          <span key={index} className={`language-item ${darkMode ? "dark-list" : ""}`}>
            {language}
            {index < Object.values(languages).length - 1 && ", "}
          </span>
        ))}
      </span>
    );
  };

  const renderBorderCountries = () => {
   
    return (
      <span className="border-countriesList">
        {borderCountries.map((country, index) => (
          <span key={index} className={`border-country-item ${darkMode ? "dark-border" : ""}`}>
            {country}
            {index < borderCountries.length - 1 && ", "}
          </span>
        ))}
      </span>
    );
  };


  return (
    <div
      className="countries-details"
      style={{
        backgroundColor: darkMode ? "#2B3743" : "white",
        color: darkMode ? "white" : "black",
        minHeight: "100vh",
      }}>
      <header className="header-details">
        <div className="countries-heading">
          <h2>Where in the world?</h2>
        </div>
        <div className="dark-Mode" onClick={toggleDarkMode}>
          <i className="fa-regular fa-moon"></i>
          <div className="darkMode">Dark Mode</div>
        </div>
      </header>
      <div className="back-homepage" onClick={() => window.history.back()}>
        <i className={`fa-solid fa-arrow-left ${darkMode ? "dark-solid" : ""}`}></i>
        <button className={`back-button ${darkMode ? "dark-button" : ""}`}>Back</button>
      </div>

      <div className="countriesDetails">
        <div className="country-image">
          <img src={country.flags.png} alt={country.name.common} />
        </div>
        <div className="countrydetails">
          

          <div className="countries-info">
          <h1 className="country-name">{country.name.common}</h1>
            <p>Native Name: {renderNativeNames()}</p>
            <p>
              Population:{" "}
              <span className={`population ${darkMode ? "dark-list" : ""}`}>
                {country.population.toLocaleString()}
              </span>
            </p>
            <p>
              Region: <span className={`region ${darkMode ? "dark-list" : ""}`}>{country.region}</span>
            </p>
            <p>
              Subregion:{" "}
              <span className={`subregion ${darkMode ? "dark-list" : ""}`}>
                {country.subregion ? country.subregion : "N/A"}
              </span>
            </p>
            <p>
              Capital: <span className={`capital ${darkMode ? "dark-list" : ""}`}>{country.capital}</span>
            </p>
          </div>
          
          <div className="additional-info">
          <p>Top Level Domain: {renderTopLevelDomains()}</p>
          <p>Currencies: {renderCurrencies()}</p>
          <p>Languages: {renderLanguages()}</p>
        </div>
          <div className={`border-countries ${darkMode ? "dark-borderlist" : ""}`}> <p>Border Countries: {renderBorderCountries()}</p></div>
        </div>
       
      </div>
    </div>
  );
}

export default CountryDetails;
