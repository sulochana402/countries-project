import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function FilterBySubregion({ region, darkMode, onSubregionChange }) {
  const [subregions, setSubregions] = useState([]);

  useEffect(() => {
    if (region) {
      fetch(`https://restcountries.com/v3.1/region/${region}`)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Failed to fetch subregions data');
          }
          return response.json();
        })
        .then((data) => {
          // console.log(data);
          const subregionMap = data.reduce((acc, country) => {
            if (country.subregion) {
              acc[country.subregion] = true;
            }
            return acc;
          }, {});
          setSubregions(Object.keys(subregionMap));
        })
        .catch((error) => console.error(error));
    } else {
      setSubregions([]);
    }
  }, [region]);

  return (
    <div className="filterBySubregion">
      <select
        onChange={onSubregionChange}
        disabled={!region}
        style={{
          backgroundColor: darkMode ? '#2B3743' : 'white',
          color: darkMode ? 'white' : 'black',
          boxShadow:"0px 0px 10px 2px rgba(0, 0, 0, 0.2)",
          height: '3rem',
          width: '10rem',
          border: 'none',
          outline: 'none',
          borderRadius:'6px'
        }}
      >
        <option value="">Filter by Subregion</option>
        {subregions.map((sub) => (
          <option key={sub} value={sub}>
            {sub}
          </option>
        ))}
      </select>
    </div>
  );
}

FilterBySubregion.propTypes = {
  region: PropTypes.string.isRequired,
  darkMode: PropTypes.bool.isRequired,
  onSubregionChange: PropTypes.func.isRequired,
};

export default FilterBySubregion;
