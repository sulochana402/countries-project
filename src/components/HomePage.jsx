import { useContext, useState } from "react";
import Countries from "./Countries";
import { DarkModeContext } from "./DarkModeContext";
import FilterBySubregion from "./FilterBySubregion";

function HomePage() {
  const [searchTerm, setSearchTerm] = useState("");
  const [region, setRegion] = useState("");
  const [subregion, setSubregion] = useState("");
  const [sortByPopulation, setSortByPopulation] = useState("");
  const [sortByArea, setSortByArea] = useState("");
  const { darkMode, toggleDarkMode } = useContext(DarkModeContext);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleRegionChange = (event) => {
    setRegion(event.target.value);
    setSubregion('');
  };

  const handleSubregionChange = (event) => {
    setSubregion(event.target.value);
  };

  const handlePopulationSortChange = (event) => {
    setSortByPopulation(event.target.value);
  };

  const handleAreaSortChange = (event) => {
    setSortByArea(event.target.value);
  };

  return (
    <div
      className="homePage"
      style={{
        backgroundColor: darkMode ? "#2B3743" : "white",
        color: darkMode ? "white" : "black",
        boxShadow: darkMode ? "none" : "0px 0px 10px 2px rgba(0, 0, 0, 0.2)",
        minHeight: "100vh",
      }}
    >
      <header className="header">
        <div className="heading">
          <h2>Where in the world?</h2>
        </div>
        <div className="dark-icon" onClick={toggleDarkMode}>
          <i className="fa-regular fa-moon"></i>
          <div className="darkMode">Dark Mode</div>
        </div>
      </header>

      <section className="filter-section">
        <div className="search-filter">
          <i
            className={`fa-solid fa-magnifying-glass ${
              darkMode ? "dark-solid" : ""
            }`}
          ></i>
          <input
            type="text"
            placeholder="Search for a country..."
            className={`input-search ${darkMode ? "dark-input" : ""}`}
            value={searchTerm}
            onChange={handleSearchChange}
          />
        </div>

        <div className="sort-population">
          <select
            value={sortByPopulation}
            onChange={handlePopulationSortChange}
            style={{
              backgroundColor: darkMode ? '#2B3743' : 'white',
              color: darkMode ? 'white' : 'black',
              boxshadow: '0px 0px 10px 2px hsla(0, 0%, 0%, 0.2)',
              height: '3rem',
              width: '10rem',
              border: 'none',
              outline: 'none',
              borderRadius:'1rem'
            }}
          >
            <option value="">Sort by Population</option>
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
          </select>
          </div>
          <div className="sort-area">
          <select
            value={sortByArea}
            onChange={handleAreaSortChange}
            style={{
              backgroundColor: darkMode ? '#2B3743' : 'white',
              color: darkMode ? 'white' : 'black',
              boxshadow: '0px 0px 10px 2px hsla(0, 0%, 0%, 0.2)',
              height: '3rem',
              width: '10rem',
              border: 'none',
              outline: 'none',
            }}
          >
            <option value="">Sort by Area</option>
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
          </select>
        </div>

        <FilterBySubregion
          region={region}
          darkMode={darkMode}
          onSubregionChange={handleSubregionChange}
        />

        <div className="select-filter">
          <select
            name=""
            className={`select-region ${darkMode ? "dark-select" : ""}`}
            value={region}
            onChange={handleRegionChange}
          >
            <option value="">Filter by Region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
      </section>

      <Countries
        searchTerm={searchTerm}
        region={region}
        subregion={subregion}
        sortByPopulation={sortByPopulation}
        sortByArea={sortByArea}
      />
    </div>
  );
}

export default HomePage;
