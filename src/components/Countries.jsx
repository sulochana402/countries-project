import { Link } from "react-router-dom";
import { useState, useEffect,useContext } from "react";
import PropTypes from "prop-types";
import { DarkModeContext } from "./DarkModeContext";
import "./countries.css";

function Countries({
  searchTerm,
  region,
  subregion,
  sortByPopulation,
  sortByArea,
}) {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const { darkMode } = useContext(DarkModeContext);

  useEffect(() => {
    setLoading(true);
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Failed to fetch countries data");
        }
        return response.json();
      })
      .then((data) => {
        setCountries(data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }, []);

  // Check for loading and error states
  if (loading) {
    return <div className="loadingState">Loading...</div>;
  }

  if (error) {
    return <div className="errorState">Error: {error.message}</div>;
  }

  // Filter countries based on searchTerm, region, and subregion
  const filteredCountries = countries.filter(
    (country) =>
      country.name.common
        .toLowerCase()
        .includes((searchTerm || "").toLowerCase()) &&
      (region === "" ||
        country.region.toLowerCase() === (region || "").toLowerCase()) &&
      (subregion === "" || country.subregion === subregion)
  );

  // Sort filtered countries
  let sortedCountries = [...filteredCountries].sort((a, b) => {
    if (sortByPopulation === "asc") {
      return a.population - b.population;
    } else if (sortByPopulation === "desc") {
      return b.population - a.population;
    }
    return 0;
  });

  // Further sort by area
  sortedCountries.sort((a, b) => {
    if (sortByArea === "asc") {
      return a.area - b.area;
    } else if (sortByArea === "desc") {
      return b.area - a.area;
    }
    return 0;
  });

  return (
    <div  className={`countriesPage ${darkMode ? "darkMode" : ""}`}
    style={{
      backgroundColor: darkMode ? "#2B3743" : "white",
      color: darkMode ? "white" : "black",
    }}>
      {sortedCountries.length === 0 ? (
        <div className="emptyState">No such countries found.</div>
      ) : (
        <section className="countries" >
          {sortedCountries.map((country) => (
            <Link
              key={country.ccn3}
              to={`/country/${country.ccn3}`}
              className={`countryLink ${darkMode ? "dark-link" : ""}`}
            >
              <div className="country" >
                <img
                  src={country.flags.png}
                  alt={country.name.common}
                  className="countriesImage"
                />
                <h2 className="countryName">{country.name.common}</h2>
                <div className="countryDetails">
                  <p>Population: 
                    <span className={`population ${darkMode ? "dark-lists" : ""}`}>{country.population.toLocaleString()}</span>
                    </p>
                  <p> Area:
                    <span className={`area ${darkMode ? "dark-lists" : ""}`}> {country.area} </span>
                  </p>
                  <p>Region: <span className={`region ${darkMode ? "dark-lists" : ""}`}>{country.region}</span></p>
                  <p> Subregion: 
                    <span className={`subregion ${darkMode ? "dark-lists" : ""}`}>{country.subregion ? country.subregion : "N/A"}</span>
                  </p>
                  <p>Capital: <span className={`capital ${darkMode ? "dark-lists" : ""}`}>{country.capital}</span></p>
                </div>
              </div>
            </Link>
          ))}
        </section>
      )}
    </div>
  );
}

Countries.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  region: PropTypes.string.isRequired,
  subregion: PropTypes.string.isRequired,
  sortByPopulation: PropTypes.oneOf(["asc", "desc", ""]).isRequired,
  sortByArea: PropTypes.oneOf(["asc", "desc", ""]).isRequired,
};

export default Countries;
